import React from 'react'

const Navbar = (props) => {
    return (
        <div>
            <nav className="navbar navbar-dark bg-dark">
                <div className="container">
                    <a href="#" className="navbar-brand">
                        <i className={props?.icon}> {props?.title} </i>
                    </a>
                </div>
            </nav>
        </div>
    )
}

export default Navbar
