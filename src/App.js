import './App.css';
import Navbar from './Navbar';
import Search from './Search';
import Alert from './Alert';
import { useState } from 'react';
import axios from 'axios'
import Users from './Users';
function App() {
  const [users, setUsers] = useState([]);
  const [alert, setAlert] = useState(null)
  const searchUsers = (query) => {
    axios
      .get(`https://api.github.com/search/users?q=${query}`)
      .then((res) =>
        setUsers(res.data.items)
      );
  }

  const clearUsers = () => {
    setUsers([]);
  }

  const alertSet = (msg, type) => {
    setAlert({ msg, type })
    setTimeout(() => {
      setAlert({ alert: null })
    }, 2000);
  }

  return (
    <>
      <Navbar title={'Github Finder'} icon={'fab fa-github'} />
      <Search setAlert={alertSet} searchUsers={searchUsers} clearUsers={clearUsers} showBtn={users.length > 0 ? true : false} />
      <Alert alert={alert} />
      <Users users={users} />
    </>
  );
}


export default App;