import React from 'react'
import User from '../User'

const Users = (props) => {
    return (
        <div className='container mt-3'>
            <div className="row">
                {props.users.map(user => (
                    <User user={user} key={user.id} />
                ))}
            </div>
        </div>
    )
}

export default Users