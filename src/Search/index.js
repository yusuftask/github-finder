import React, { useState } from 'react'

function Search(props) {
    const [query, setQuery] = useState('')

    const handleSubmit = (e) => {
        console.log(e);
        e.preventDefault();
        if (query === "") {
            props.setAlert("lütfen bir anahtar kelime giriniz !", "danger");
        } else {
            props.searchUsers(query);
            setQuery('')
        }
    }
    return (
        <div className="container my-3">
            <form onSubmit={handleSubmit}>
                <div className="input-group">
                    <input
                        value={query}
                        type="text"
                        onChange={(e) => setQuery(e.target.value)}
                        className="form-control"
                    />
                    <button type="submit" className="btn-dark btn-sm">
                        Search
                    </button>
                </div>
                {props.showBtn && (
                    <button
                        onClick={props.clearUsers}
                        className="btn btn-secondary mt-2 btn-sm btn-block"
                    >
                        Clear Result
                    </button>
                )}
            </form>

        </div>
    )
}

export default Search